package pkg3dproject.shooter3D;

import pkg3dproject.math3D.*;
import pkg3dproject.game.GameObject;

/**
    Klaas Blast, kt�ra dziedziczy z klasy GameObject, reprezentuje
    pocisk, kt�ry porusza si� w lini prostej przez pi�� sekund, po
    czym znika. Pociski natychmiast niszcz� trafione boty.
*/
public class Blast extends GameObject {

    private static final long DIE_TIME = 5000;
    private static final float SPEED = 1.5f;
    private static final float ROT_SPEED = .008f;

    private long aliveTime;

    /**
        Tworzy nowy obiekt klasy Blast z przekazanym obiekte klasy
        PolygonGroup i znormalizowanym wektorem kierunku.
    */
    public Blast(PolygonGroup polygonGroup, Vector3D direction) {
        super(polygonGroup);
        MovingTransform3D transform = polygonGroup.getTransform();
        Vector3D velocity = transform.getVelocity();
        velocity.setTo(direction);
        velocity.multiply(SPEED);
        transform.setVelocity(velocity);
        //transform.setAngleVelocityX(ROT_SPEED);
        transform.setAngleVelocityY(ROT_SPEED);
        transform.setAngleVelocityZ(ROT_SPEED);
        setState(STATE_ACTIVE);
    }


    public void update(GameObject player, long elapsedTime) {
        aliveTime+=elapsedTime;
        if (aliveTime >= DIE_TIME) {
            setState(STATE_DESTROYED);
        }
        else {
            super.update(player, elapsedTime);
        }
    }

}
