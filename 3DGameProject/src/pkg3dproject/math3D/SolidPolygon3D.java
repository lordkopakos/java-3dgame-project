package pkg3dproject.math3D;

import java.awt.Color;

/*
    Klasa SolidPolygon3D to klasa wielokąta Polygon wzbogacona o kolor.
 */
public class SolidPolygon3D extends Polygon3D {

    private Color color = Color.GREEN;

    public SolidPolygon3D() {
        super();
    }


    public SolidPolygon3D(Vector3D v0, Vector3D v1, Vector3D v2) {
        this(new Vector3D[] { v0, v1, v2 });
    }


    public SolidPolygon3D(Vector3D v0, Vector3D v1, Vector3D v2,
        Vector3D v3)
    {
        this(new Vector3D[] { v0, v1, v2, v3 });
    }


    public SolidPolygon3D(Vector3D[] vertices) {
        super(vertices);
    }


    public void setTo(Polygon3D polygon) {
        super.setTo(polygon);
        if (polygon instanceof SolidPolygon3D) {
            color = ((SolidPolygon3D)polygon).color;
        }
    }


    /*
        Pobiera kolor tego jednolicie wypełnionego wielokąta
        używany do wypełnienia tegoż wielokąta.
    */
    public Color getColor() {
        return color;
    }


    /*
        Definiuje jednolity kolor tego wielokąta, którym będzie on
        wypełniany podczas renderowania.
    */
    public void setColor(Color color) {
        this.color = color;
    }

}
