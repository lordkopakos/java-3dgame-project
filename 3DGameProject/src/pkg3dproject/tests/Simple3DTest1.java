/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3dproject.tests;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.geom.GeneralPath;
import pkg3dproject.core.GameCore;
import pkg3dproject.math3D.*;
import pkg3dproject.input.*;

public class Simple3DTest1 extends GameCore{

public static void main(String[] args){
    new Simple3DTest1().run();
}     

//Utwórz wielokąt
private SolidPolygon3D treeLeaves= new SolidPolygon3D(
        new Vector3D(-50, -35, 0), 
        new Vector3D(50, -35, 0), 
        new Vector3D(0, 200, 0));

private SolidPolygon3D treeTrunk=new SolidPolygon3D(
        new Vector3D(-5, -50, 0), 
        new Vector3D(5, -50, 0), 
        new Vector3D(5, -35, 0), 
        new Vector3D(-5, -35, 0));

private Transform3D treeTransform=new Transform3D(0, 0, -500);
private Polygon3D transformedPolygon=new Polygon3D();
private ViewWindow viewWindow;

private GameAction exit=new GameAction("exit");
private GameAction zoomIn=new GameAction("zoomIn");
private GameAction zoomOut=new GameAction("zoomOut");

    
    @Override
    public void init(){
        super.init();
        InputManager inputManager=new InputManager(screen.getFullScreenWindow());
        inputManager.setCursor(InputManager.INVISIBLE_CURSOR);
        inputManager.mapToKey(exit, KeyEvent.VK_ESCAPE);
        inputManager.mapToKey(zoomIn, KeyEvent.VK_UP);
        inputManager.mapToKey(zoomOut, KeyEvent.VK_DOWN);
        
        //Niech okno obrazu zajmuje cały ekran
        viewWindow=new ViewWindow(0, 0, screen.getWidth(), screen.getHeight(), (float)Math.toRadians(75));
        
        //Wypełnij wielokąty kolorami
        treeLeaves.setColor(new Color(0x008000));
        treeTrunk.setColor(new Color(0x714311));
    }
    @Override
    public void update(long elapsedTime){
       if(exit.isPressed()){
           stop();
           return;
       }
       //Przechwycić czas elapsedTime
       elapsedTime=Math.min(elapsedTime, 100);
       //Obróć na około osi Y
       treeTransform.rotateAngleY(0.002f*elapsedTime);
       //Pozwól użytkowinikowi powiększyć i pomniejszyć;
       if(zoomIn.isPressed()){
           treeTransform.getLocation().z+=0.5f*elapsedTime;
       }
       if(zoomOut.isPressed()){
           treeTransform.getLocation().z-=0.5f*elapsedTime;
       }
    }
    @Override
    public void draw(Graphics2D g){
        //Usuń tło
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, screen.getWidth(), screen.getHeight());
        
        //Wyświetl komunikat
        g.setColor(Color.WHITE);
        g.drawString("Klawisze UP/DOWN to regulacja powiększenia", 5, fontSize);
        
        //Narysuj wielokąty
        transformAndDraw(g, treeTrunk);
        transformAndDraw(g, treeLeaves);
    }
   private void transformAndDraw(Graphics2D g, SolidPolygon3D poly){
       transformedPolygon.setTo(poly);
       
       //Wykonaj translację i o0brót wielokąta
       transformedPolygon.add(treeTransform);
       
       //Rzutuj wielokąt na ekran;
       transformedPolygon.project(viewWindow);
       
       //Konwertuj obiekt na Java2D GeneralPath i narysuj go
       GeneralPath path= new GeneralPath();
       Vector3D v=transformedPolygon.getVertex(0);
       path.moveTo(v.x, v.y);
       for(int i=1; i<transformedPolygon.getNumVertices(); i++){
           v=transformedPolygon.getVertex(i);
           path.lineTo(v.x, v.y);
       }
       g.setColor(poly.getColor());
       g.fill(path);
   }
}
