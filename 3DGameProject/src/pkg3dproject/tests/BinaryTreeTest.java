package pkg3dproject.tests;

public class BinaryTreeTest {

    public static void main(String[] args) {
        new BinaryTreeTest().run();
    }

    static class Node {
        Node left;
        Node right;
        int value;

        public Node(int value) {
            this.value = value;
        }
    }

    public void run() {
        // zbuduj proste drzewo z rozdzia�u 11.
        Node root = new Node(5);
        System.out.println("Przyk�ad drzewa binarnego");
        System.out.println("Budowa drzewa z warto�ci� w korzeniu: " +
            root.value);
        insert(root, 1);
        insert(root, 8);
        insert(root, 6);
        insert(root, 3);
        insert(root, 9);
        System.out.println("Poprzeczne przeszukiwanie drzewa");
        printInOrder(root);
        System.out.println("Przeszukiwanie drzewa od przodu do ty�u od po�o�enia 7");
        printFrontToBack(root, 7);
    }


    public void insert(Node node, int value) {
        if (value < node.value) {
            if (node.left != null) {
                insert(node.left, value);
            }
            else {
                System.out.println("  Inserted " + value +
                    " to left of " + node.value);
                node.left = new Node(value);
            }
        }
        else if (value > node.value) {
            if (node.right != null) {
                insert(node.right, value);
            }
            else {
                System.out.println("  Inserted " + value +
                    " to right of " + node.value);
                node.right = new Node(value);
            }
        }
    }


    public void printInOrder(Node node) {
        if (node != null) {
            printInOrder(node.left);
            System.out.println("  Odwiedzony w�ze�: " + node.value);
            printInOrder(node.right);
        }
    }

    /**
        U�ywa metody przegl�dania poprzecznego, je�li przodek jest mniejszy od
        warto�ci potomka.

        U�ywa metody przegl�dania wstecznego, je�li przodek jest wi�kszy od
        warto�ci potomka.
    */
    public void printFrontToBack(Node node, int camera) {
        if (node == null) return;
        if (node.value > camera) {
            // wy�wietla w kolejno�ci poprzecznej
            printFrontToBack(node.left, camera);
            System.out.println("  Odwiedzony w�ze�: " + node.value);
            printFrontToBack(node.right, camera);
        }
        else if (node.value < camera) {
            // wy�wietla w kolejno�ci wstecznej
            printFrontToBack(node.right, camera);
            System.out.println("  Odwiedzony w�ze�: " + node.value);
            printFrontToBack(node.left, camera);
        }
        else {
            // kolejno�� nie ma znaczenia
            printFrontToBack(node.left, camera);
            printFrontToBack(node.right, camera);
        }
    }


}
