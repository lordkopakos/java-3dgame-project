package pkg3dproject.bsp2D;

import java.io.*;
import java.util.*;
import pkg3dproject.math3D.*;

/**
    Klasa MapLoader wczytuje mapy z pliku tekstowego zbudowanego
    zgodnie z ze specyfikacj� plik�w OBJ Alias|Wavefront.
*/
public class MapLoader extends ObjectLoader {

    private BSPTreeBuilder builder;
    private Map loadedObjects;
    private Transform3D playerStart;
    private RoomDef currentRoom;
    private List rooms;
    private List mapObjects;

    // u�yj osobnego obiektu klasy ObjectLoader dla obiekt�w
    private ObjectLoader objectLoader;


    /**
        Tworzy nowy obiekt klasy MapLoader wykorzystuj�cy domy�lny obiekt BSPTreeBuilder.
    */
    public MapLoader() {
        this(null);
    }


    /**
        Tworzy nowy obiekt klasy MapLoader wykorzystuj�cy wskazany obiekt BSPTreeBuilder.
        Je�li wskazany obiekt jest r�wny null, tworzony jest domy�lny obiekt BSPTreeBuilder.
    */
    public MapLoader(BSPTreeBuilder builder) {
        if (builder == null) {
            this.builder = new BSPTreeBuilder();
        }
        else {
            this.builder = builder;
        }
        parsers.put("map", new MapLineParser());
        objectLoader = new ObjectLoader();
        loadedObjects = new HashMap();
        rooms = new ArrayList();
        mapObjects = new ArrayList();
    }


    /**
        Wczytuje plik mapy i tworzy drzewo BSP. Tworzone obiekty mo�na odczytywa� za
        pomoc� metody getObjectsInMap().
    */
    public BSPTree loadMap(String filename) throws IOException {
        currentRoom = null;
        rooms.clear();
        vertices.clear();
        mapObjects.clear();
        playerStart = new Transform3D();

        path = new File(filename).getParentFile();

        parseFile(filename);

        return createBSPTree();
    }


    /**
        Tworzy drzewo BSP na podstawie pomieszcze� zdefiniowanych w pliku mapy.
    */
    protected BSPTree createBSPTree() {
        // odczytaj wszystkie wielok�ty
        List allPolygons = new ArrayList();
        for (int i=0; i<rooms.size(); i++) {
            RoomDef room = (RoomDef)rooms.get(i);
            allPolygons.addAll(room.createPolygons());
        }

        // buduj drzewo
        BSPTree tree = builder.build(allPolygons);

        // stw�rz powierzchnie wielok�t�w z uwzgl�dnieniem �wiate�
        tree.createSurfaces(lights);
        return tree;
    }


    /**
        Zwraca list� wszystkich obiekt�w zdefiniowanych w pliku mapy.
    */
    public List getObjectsInMap() {
        return mapObjects;
    }


    /**
        Zwraca pocz�tkowe po�o�enie gracza zdefiniowane w pliku mapy.
    */
    public Transform3D getPlayerStartLocation() {
        return playerStart;
    }


    /**
        Ustawia �wiat�a wykorzystywane dla obiekt�w OBJ.
    */
    public void setObjectLights(List lights,
        float ambientLightIntensity)
    {
        objectLoader.setLights(lights, ambientLightIntensity);
    }


    /**
        Parsuje pojedynczy wiersz z pliku MAP.
    */
    protected class MapLineParser implements LineParser {

        public void parseLine(String line) throws IOException,
            NoSuchElementException
        {
            StringTokenizer tokenizer = new StringTokenizer(line);
            String command = tokenizer.nextToken();

            if (command.equals("v")) {
                // stw�rz nowy wierzcho�ek
                vertices.add(new Vector3D(
                    Float.parseFloat(tokenizer.nextToken()),
                    Float.parseFloat(tokenizer.nextToken()),
                    Float.parseFloat(tokenizer.nextToken())));
            }
            else if (command.equals("mtllib")) {
                // wczytaj materia�y z pliku
                String name = tokenizer.nextToken();
                parseFile(name);
            }
            else if (command.equals("usemtl")) {
                // zdefiniuj bie��cy materia�
                String name = tokenizer.nextToken();
                if ("null".equals(name)) {
                    currentMaterial = new Material();
                }
                else {
                    currentMaterial =
                        (Material)materials.get(name);
                    if (currentMaterial == null) {
                        currentMaterial = new Material();
                        System.out.println("brak materia�u: " + name);
                    }
                }
            }
            else if (command.equals("pointlight")) {
                // stw�rz �wiat�o punktowe
                Vector3D loc = getVector(tokenizer.nextToken());
                float intensity = 1;
                float falloff = PointLight3D.NO_DISTANCE_FALLOFF;
                if (tokenizer.hasMoreTokens()) {
                    intensity =
                        Float.parseFloat(tokenizer.nextToken());
                }
                if (tokenizer.hasMoreTokens()) {
                    falloff =
                        Float.parseFloat(tokenizer.nextToken());
                }
                lights.add(new PointLight3D(loc.x, loc.y, loc.z,
                    intensity, falloff));
            }
            else if (command.equals("ambientLightIntensity")) {
                // zdefiniuj intensywno�� �wiat�a otaczaj�cego
                ambientLightIntensity =
                    Float.parseFloat(tokenizer.nextToken());
            }
            else if (command.equals("player")) {
                // zdefiniuj pocz�tkowe po�o�enie gracza
                playerStart.getLocation().setTo(
                    getVector(tokenizer.nextToken()));
                if (tokenizer.hasMoreTokens()) {
                    playerStart.setAngleY(
                        Float.parseFloat(tokenizer.nextToken()));
                }
            }
            else if (command.equals("obj")) {
                // stw�rz nowy obiekt
                String uniqueName = tokenizer.nextToken();
                String filename = tokenizer.nextToken();
                // sprawd�, czy obiekt nie zosta� ju� wczytany
                PolygonGroup object =
                    (PolygonGroup)loadedObjects.get(filename);
                if (object == null) {
                    File file = new File(path, filename);
                    String filePath = file.getPath();
                    object = objectLoader.loadObject(filePath);
                    loadedObjects.put(filename, object);
                }
                Vector3D loc = getVector(tokenizer.nextToken());
                PolygonGroup mapObject =
                    (PolygonGroup)object.clone();
                mapObject.getTransform().getLocation().setTo(loc);
                if (!uniqueName.equals("null")) {
                    mapObject.setName(uniqueName);
                }
                if (tokenizer.hasMoreTokens()) {
                    mapObject.getTransform().setAngleY(
                        Float.parseFloat(tokenizer.nextToken()));
                }
                mapObjects.add(mapObject);
            }
            else if (command.equals("room")) {
                // rozpocznij wczytywanie nowego pomieszczenia
                currentRoom = new RoomDef(ambientLightIntensity);
                rooms.add(currentRoom);
            }
            else if (command.equals("floor")) {
                // zdefiniuj pod�og� w pomieszczeniu
                float y = Float.parseFloat(tokenizer.nextToken());
                currentRoom.setFloor(y, currentMaterial.texture);
            }
            else if (command.equals("ceil")) {
                // zdefiniuj sufit w pomieszczeniu
                float y = Float.parseFloat(tokenizer.nextToken());
                currentRoom.setCeil(y, currentMaterial.texture);
            }
            else if (command.equals("wall")) {
                // zdefiniuj wierzcho�ek �ciany w pomieszczeniu
                float x = Float.parseFloat(tokenizer.nextToken());
                float z = Float.parseFloat(tokenizer.nextToken());
                if (tokenizer.hasMoreTokens()) {
                    float bottom =
                        Float.parseFloat(tokenizer.nextToken());
                    float top =
                        Float.parseFloat(tokenizer.nextToken());
                    currentRoom.addVertex(x, z, bottom, top,
                        currentMaterial.texture);
                }
                else {
                    currentRoom.addVertex(x, z,
                        currentMaterial.texture);
                }
            }
            else {
                System.out.println("Nienana instrukcja: " + command);
            }
        }
    }
}