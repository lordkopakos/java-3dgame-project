package pkg3dproject.bsp2D;

import java.awt.*;
import java.awt.image.*;
import java.util.HashMap;
import pkg3dproject.math3D.*;
import pkg3dproject.bsp2D.*;
import pkg3dproject.graphics3D.*;
import pkg3dproject.graphics3D.texture.*;
import pkg3dproject.game.GameObjectManager;

/**
    Klasa BSPRenderer odpowiada za rednerowanie rysowanych wielok�t�w,
    kt�re s� przechowywane w drzewie BSP oraz wszystkich obiekt�w z�o�onych z
    wielok�t�w. Rysuj�c te wielok�ty klasa BSPRenderer zapisuje ich g��bo�ci w
    z-buforze. Obiekty z�o�one z wielok�t�w wykorzystuj� z-bufor do okre�lania
    (na poziomie pojedynczych pikseli), czy s� widoczne na scenie.
*/
public class BSPRenderer extends ZBufferedRenderer
    implements BSPTreeTraverseListener
{

    /**
        Definiuje, ile wielok�t�w nale�y narysowa� przed sprawdzeniem wype�nienia
        ekranu.
    */
    private static final int FILLED_CHECK = 3;

    protected HashMap bspScanRenderers;
    protected BSPTreeTraverser traverser;
    protected Graphics2D currentGraphics2D;
    protected boolean viewNotFilledFirstTime;
    protected int polygonCount;

    /**
        Tworzy nowy obiekt BSPRenderer dla przekazanego obiektu kamery
        i okna widoku.
    */
    public BSPRenderer(Transform3D camera,
        ViewWindow viewWindow)
    {
        super(camera, viewWindow, false);
        viewNotFilledFirstTime = true;
        traverser = new BSPTreeTraverser(this);
    }


    /**
        Ustawia GamebjectManager. Przegl�danie drzewa BSP pozwala
        okre�li�, kt�re obiekty s� widoczne.
    */
    public void setGameObjectManager(
        GameObjectManager gameObjectManager)
    {
        traverser.setGameObjectManager(gameObjectManager);
    }


    protected void init() {
        destPolygon = new TexturedPolygon3D();
        scanConverter = new SortedScanConverter(viewWindow);

        // tworzy obiekty renderuj�ce dla wszystkich tekstur (optymalizacja HotSpot)
        scanRenderers = new HashMap();
        scanRenderers.put(PowerOf2Texture.class,
            new PowerOf2TextureZRenderer());
        scanRenderers.put(ShadedTexture.class,
            new ShadedTextureZRenderer());
        scanRenderers.put(ShadedSurface.class,
            new ShadedSurfaceZRenderer());

        // jw., ale dla wielok�t�w przechowywanych w drzewie BSP
        bspScanRenderers = new HashMap();
        bspScanRenderers.put(PowerOf2Texture.class,
            new PowerOf2TextureRenderer());
        bspScanRenderers.put(ShadedTexture.class,
            new ShadedTextureRenderer());
        bspScanRenderers.put(ShadedSurface.class,
            new ShadedSurfaceRenderer());
    }


    public void startFrame(Graphics2D g) {
        super.startFrame(g);
        ((SortedScanConverter)scanConverter).clear();
        polygonCount = 0;
    }


    public void endFrame(Graphics2D g) {
        super.endFrame(g);
        if (!((SortedScanConverter)scanConverter).isFilled()) {
            g.drawString("Ekrane nie jest wype�niony", 5,
                viewWindow.getTopOffset() +
                viewWindow.getHeight() - 5);
            if (viewNotFilledFirstTime) {
                viewNotFilledFirstTime = false;
                // wy�wietla na konsoli odpowiedni komunikat
                System.out.println("Ekran nie jest w ca�o�ci wype�niony.");
            }
            // czy�ci t�o
            clearViewEveryFrame = true;
        }
        else {
            clearViewEveryFrame = false;
        }
    }


    /**
        Rysuje widoczne wielok�ty z drzewa BSP w oparciu o po�o�enie
        kamery. Wielok�ty s� rysowane od pierwszego do ostatniego planu.
    */
    public void draw(Graphics2D g, BSPTree tree) {
        ((SortedScanConverter)scanConverter).setSortedMode(true);
        currentGraphics2D = g;
        traverser.traverse(tree, camera.getLocation());
        ((SortedScanConverter)scanConverter).setSortedMode(false);
    }


    // z interfejsu BSPTreeTraverseListener
    public boolean visitPolygon(BSPPolygon poly, boolean isBack) {
        SortedScanConverter scanConverter =
            (SortedScanConverter)this.scanConverter;

        draw(currentGraphics2D, poly);

        // co trzy wielok�ty sprawdza, czy ekran jest wype�niony
        polygonCount++;
        if (polygonCount == FILLED_CHECK) {
            polygonCount = 0;
            return
                !((SortedScanConverter)scanConverter).isFilled();
        }
        return true;
    }


    protected void drawCurrentPolygon(Graphics2D g) {
        if (!(sourcePolygon instanceof BSPPolygon)) {
            super.drawCurrentPolygon(g);
            return;
        }
        buildSurface();
        SortedScanConverter scanConverter =
            (SortedScanConverter)this.scanConverter;
        TexturedPolygon3D poly = (TexturedPolygon3D)destPolygon;
        Texture texture = poly.getTexture();
        ScanRenderer scanRenderer = (ScanRenderer)
            bspScanRenderers.get(texture.getClass());
        scanRenderer.setTexture(texture);
        Rectangle3D textureBounds = poly.getTextureBounds();

        a.setToCrossProduct(textureBounds.getDirectionV(),
            textureBounds.getOrigin());
        b.setToCrossProduct(textureBounds.getOrigin(),
            textureBounds.getDirectionU());
        c.setToCrossProduct(textureBounds.getDirectionU(),
            textureBounds.getDirectionV());

        // zmienna w wykorzystywana do obliczania g��boko�ci ka�dego piksela
        w = SCALE * MIN_DISTANCE * Short.MAX_VALUE /
            (viewWindow.getDistance() *
            c.getDotProduct(textureBounds.getOrigin()));

        int y = scanConverter.getTopBoundary();
        viewPos.y = viewWindow.convertFromScreenYToViewY(y);
        viewPos.z = -viewWindow.getDistance();

        while (y<=scanConverter.getBottomBoundary()) {
            for (int i=0; i<scanConverter.getNumScans(y); i++) {
                ScanConverter.Scan scan =
                    scanConverter.getScan(y, i);

                if (scan.isValid()) {
                    viewPos.x = viewWindow.
                        convertFromScreenXToViewX(scan.left);
                    int offset = (y - viewWindow.getTopOffset()) *
                        viewWindow.getWidth() +
                        (scan.left - viewWindow.getLeftOffset());

                    scanRenderer.render(offset, scan.left,
                        scan.right);
                    setScanDepth(offset, scan.right-scan.left+1);
                }
            }
            y++;
            viewPos.y--;
        }
    }


    /**
        Ustawia z-g��boko�� dla bie��cej �cie�ki wielok�ta.
    */
    private void setScanDepth(int offset, int width) {
        float z = c.getDotProduct(viewPos);
        float dz = c.x;
        int depth = (int)(w*z);
        int dDepth = (int)(w*dz);
        short[] depthBuffer = zBuffer.getArray();
        int endOffset = offset + width;

        // ustawiona g��bia b�dzie sta�a dla wielu pod��g i sufit�w
        if (dDepth == 0) {
            short d = (short)(depth >> SCALE_BITS);
            while (offset < endOffset) {
                depthBuffer[offset++] = d;
            }
        }
        else {
            while (offset < endOffset) {
                depthBuffer[offset++] =
                    (short)(depth >> SCALE_BITS);
                depth += dDepth;
            }
        }
    }

}
