package pkg3dproject.bsp2D;

import pkg3dproject.math3D.*;

/**
    Klasa BSPPolygon jest rozszerzeniem klasy TexturedPolygon3D
    z dodatkowymi sta�ymi typu (TYPE_FLOOR, TYPE_WALL lub 
    TYPE_PASSABLE_WALL), warto�ci� intensywno�ci �wiat�a
    otoczenia i obiektem klasy BSPLine (je�li typem �ciany jest
    TYPE_WALL lub TYPE_PASSABLE_WALL).
*/
public class BSPPolygon extends TexturedPolygon3D {

    public static final int TYPE_FLOOR = 0;
    public static final int TYPE_WALL = 1;
    public static final int TYPE_PASSABLE_WALL = 2;

    /**
        Definiuje odleg�o��, w jakiej musi si� znajdowa� �ciana, by
        potw�ry i gracze mogli j� przekracza�.
    */
    public static final int PASSABLE_WALL_THRESHOLD = 32;

    /**
        Definiuje wysoko�� przej�cia, kt�ra umo�liwia potw�rom i graczom
        jego pokonywanie.
    */
    public static final int PASSABLE_ENTRYWAY_THRESHOLD = 128;


    private int type;
    private float ambientLightIntensity;
    private BSPLine line;

    /**
        Tworzy nowy obiekt BSPPolygon dla przekazanych wierzcho�k�w
        i typu (TYPE_FLOOR, TYPE_WALL lub TYPE_PASSABLE_WALL).
    */
    public BSPPolygon(Vector3D[] vertices, int type) {
        super(vertices);
        this.type = type;
        ambientLightIntensity = 0.5f;
        if (isWall()) {
            line = new BSPLine(this);
        }
    }


    /**
        Powiela ten wielok�t, jednak z innym zbiorem wierzcho�k�w.
    */
    public BSPPolygon clone(Vector3D[] vertices) {
        BSPPolygon clone = new BSPPolygon(vertices, type);
        clone.setNormal(getNormal());
        clone.setAmbientLightIntensity(getAmbientLightIntensity());
        if (getTexture() != null) {
            clone.setTexture(getTexture(), getTextureBounds());
        }
        return clone;
    }


    /**
        Zwraca true, je�li ten obiekt BSPPolygon reprezentuje �cian�.
    */
    public boolean isWall() {
        return (type == TYPE_WALL) || (type == TYPE_PASSABLE_WALL);
    }


    /**
        Zwraca true, je�li ten obiekt BSPPolygon reprezentuje �cian�
        uniemo�liwiaj�c� przej�cie.
    */
    public boolean isSolidWall() {
        return type == TYPE_WALL;
    }


    /**
        Zwraca lini� reprezentuj�c� ten obiekt BSPPolygon. Zwraca null,
        je�li ten obiekt BSPPolygon nie jest �cian�.
    */
    public BSPLine getLine() {
        return line;
    }


    public void setAmbientLightIntensity(float a) {
        ambientLightIntensity = a;
    }


    public float getAmbientLightIntensity() {
        return ambientLightIntensity;
    }

}
