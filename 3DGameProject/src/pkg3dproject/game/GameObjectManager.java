package pkg3dproject.game;

import java.awt.Rectangle;
import java.awt.Graphics2D;
import pkg3dproject.game.GameObjectRenderer;

/*
    Interfejs GameObjectManager zawiera metody umożliwiające
    śledzenie i rysowanie obiektów GameObject.
*/
public interface GameObjectManager {


    /*
        Oznacza wszystkie obiekty znajdujące się w wyznaczonym
        dwuwymiarowym obszarze jako potencjalnie widoczne (czyli
        takie, które należy narysować).
    */
    public void markVisible(Rectangle bounds);


    /*
        Oznacza wszystkie obiekty jako potencjalnie widoczne (czyli
        takie, które należy narysować).
    */
    public void markAllVisible();


    /*
        Dodaje obiekt klasy GameObject do tego menadżera.
    */
    public void add(GameObject object);


    /*
        Dodaje obiekt klasy GameObject do tego menadżera i określa
        go jako obiekt gracza. Istniejący obiekt gracza, jeśli istnieje,
        nie jest usuwany.
    */
    public void addPlayer(GameObject player);


    /*
        Zwraca obiekt określony jako obiekt gracza lub wartość null,
        jeśli nie wyznaczono żadnego obiektu gracza.
    */
    public GameObject getPlayer();


    /*
        Usuwa obiekt klasy GameObject z tego menadżera.
    */
    public void remove(GameObject object);


    /*
        Aktualizuje wszystkie obiekty w oparciu o czas, jaki minął
        od ostatniej aktualizacji.
    */
    public void update(long elapsedTime);


    /*
        Rysuje wszystkie widoczne obiekty.
    */
    public void draw(Graphics2D g, GameObjectRenderer r);


}
