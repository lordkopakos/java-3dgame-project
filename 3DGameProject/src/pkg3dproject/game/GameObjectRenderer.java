package pkg3dproject.game;

import java.awt.Graphics2D;
import pkg3dproject.game.GameObject;

/*
    Interfejs GameObjectRenderer udostępnia metody rysowania
    obiektu GameObject.
*/
public interface GameObjectRenderer {

    /*
        Rysuje obiekt i zwraca wartość true, jeśli którakolwiek
        część obiektu jest widoczna.
    */
    public boolean draw(Graphics2D g, GameObject object);

}
